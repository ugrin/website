-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 21, 2018 at 02:58 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Website`
--

-- --------------------------------------------------------

--
-- Table structure for table `About`
--

CREATE TABLE `About` (
  `id` int(11) NOT NULL,
  `link_cover` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `description` text,
  `moreInfo` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `About`
--

INSERT INTO `About` (`id`, `link_cover`, `title`, `subtitle`, `description`, `moreInfo`) VALUES
(70, 'https://uploads.dailyvanity.sg/wp-content/uploads/2016/11/natural-ingredients.jpg', 'Earth Life Organics', 'The Goodness of Nature ', '\r\nEarth Life Organics offers a breakthrough concept in body care which gives balance to the body, mind and soul. Using its knowledge in aromatherapy, homeopathy and herbal therapy, we create body, hair and facial care products with the infusion of botanics, essential oils and a range of aromas that invigorate your senses and uplift your spirit.\r\n\r\nEstablished by Body Care experts with more than two decades of experience, Earth Life Organics is a trusted purveyor of all-natural skincare products, made with phyto-ingredients sourced from South East Asiaâ€™s herbal garden, Indonesia, one of the most botanically gifted places in the world.\r\n\r\nWe use a tried and tested extraction process which ensures the preservation of natural phyto-nutrients and antioxidants in our products, essential in making them so singularly unique and at the same time commonly beneficial.', 'The brand has been established by experts with more than 20 years of experience in the field. The products are made with phyto-ingredients that are sourced from Indonesia, which is one of the most botanically rich places in the world.\r\n\r\nâ€œBecause we are using some of the finest botanical ingredients, we have to make sure our extraction method preserves their integrity; we use a tried-and-tested extraction process that allows the ingredients to retain their full benefits,â€ he adds.\r\n\r\nEarth Life Organics products are available at their online store, which has a simple order and delivery process, and is also hosted at Kitchen By Food Rebel located at 28 Stanley Street (in the Telok Ayer area).'),
(71, '', '', '', '', ''),
(72, '', '', '', '', ''),
(73, 'https://www.volkswagen.com.mk/media/Theme_Stage_Default_Image_Component/15264-stage-child-image/dh-1295-028de8/aafe2cb8/1478987257/vw-original-teile-1.jpg', 'Volkswagen', 'Ð¡Ðµ Ð¿Ð¾Ð²Ñ€Ð·ÑƒÐ²Ð° ÑÐ¾ ÑÐ²ÐµÑ‚Ð¾Ñ‚', 'Ð”Ð˜Ð Ð•ÐšÐ¢ÐÐž ÐŸÐ Ð˜ÐœÐÐŠÐ•\r\nÐžÐ´Ð²Ð¾Ñ˜ÑƒÐ²Ð°Ð¼Ðµ Ð²Ñ€ÐµÐ¼Ðµ Ð·Ð° Ð’Ð°Ñ\r\n\r\nÐ’Ð°ÑˆÐµÑ‚Ð¾ Ð²Ð¾Ð·Ð¸Ð»Ð¾ ÑÐµ Ð¿Ð¾Ð´Ð¸Ð³Ð½ÑƒÐ²Ð° Ð½Ð° Ð¿Ð»Ð°Ñ‚Ñ„Ð¾Ñ€Ð¼Ð° Ð¸ Ð’Ð¸Ðµ Ð¸ÑÑ‚Ð¾ Ñ‚Ð°ÐºÐ° Ð¼Ð¾Ð¶ÐµÑ‚Ðµ Ð´Ð° Ð³Ð¾ Ð½Ð°Ð±Ñ™ÑƒÐ´ÑƒÐ²Ð°Ñ‚Ðµ Ð¾Ð´ Ð´Ð¾Ð»Ñƒ.\r\n\r\nÐ’Ð°ÑˆÐ¸Ð¾Ñ‚ ÑÐ¾Ð²ÐµÑ‚Ð½Ð¸Ðº Ð·Ð° ÑÐµÑ€Ð²Ð¸Ñ Ð¼ÐµÑ“Ñƒ Ð´Ñ€ÑƒÐ³Ð¾Ñ‚Ð¾ ÑÐ¾ Ð’Ð°Ñ ÑœÐµ Ñ€Ð°Ð·Ð³Ð¾Ð²Ð°Ñ€Ð° Ð·Ð° ÑÐ¾ÑÑ‚Ð¾Ñ˜Ð±Ð°Ñ‚Ð° Ð½Ð° Ð³ÑƒÐ¼Ð¸Ñ‚Ðµ, ÑÐ¾Ð¿Ð¸Ñ€Ð°Ñ‡ÐºÐ¸Ñ‚Ðµ, ÑƒÑ€ÐµÐ´Ð¾Ñ‚ Ð·Ð° Ð¸Ð·Ð´ÑƒÐ²Ð½Ð¸ Ð³Ð°ÑÐ¾Ð²Ð¸ Ð¸ Ð¿Ð¾Ð´Ð²Ð¾Ð·Ñ˜ÐµÑ‚Ð¾. ÐŸÑ€Ð¾Ð²ÐµÑ€ÐºÐ°Ñ‚Ð° Ð¾Ð´ ÑÐ¸Ñ‚Ðµ ÑÑ‚Ñ€Ð°Ð½Ð¸ ÑœÐµ Ð’Ð¸ Ð´Ð°Ð´Ðµ Ñ†ÐµÐ»Ð¾ÑÐµÐ½ Ð¿Ñ€ÐµÐ³Ð»ÐµÐ´ Ð·Ð° Ð½Ð°Ð´Ð²Ð¾Ñ€ÐµÑˆÐ½Ð°Ñ‚Ð° ÑÐ¾ÑÑ‚Ð¾Ñ˜Ð±Ð°.', 'Ð”Ð¾Ð±Ð¸Ð²Ð°Ñ‚Ðµ Ð½ÐµÐ¿Ð¾ÑÑ€ÐµÐ´Ð½Ð¾ ÑÐ¾Ð²ÐµÑ‚ÑƒÐ²Ð°ÑšÐµ Ð·Ð° ÑÐ¸Ñ‚Ðµ Ð²Ð¸Ð´Ð»Ð¸Ð²Ð¸ Ð¾ÑˆÑ‚ÐµÑ‚ÑƒÐ²Ð°ÑšÐ°.\r\n\r\nÐ’Ð°ÑˆÐ¸Ð¾Ñ‚ ÑÐ¾Ð²ÐµÑ‚Ð½Ð¸Ðº Ð·Ð° ÑÐµÑ€Ð²Ð¸Ñ Ñ‡ÐµÑÑ‚Ð¾ Ð¸ Ð·Ð° Ð²Ñ€ÐµÐ¼Ðµ Ð½Ð° Ð¿Ñ€Ð¾Ð²ÐµÑ€ÐºÐ°Ñ‚Ð° Ð¼Ð¾Ð¶Ðµ Ð´Ð° Ð’Ð¸ ÐºÐ°Ð¶Ðµ, ÐºÐ¾Ð¸ Ð¾ÑˆÑ‚ÐµÑ‚ÑƒÐ²Ð°ÑšÐ° Ñ‚Ñ€ÐµÐ±Ð° Ð²ÐµÐ´Ð½Ð°Ñˆ Ð´Ð° ÑÐµ Ð¾Ñ‚ÑÑ‚Ñ€Ð°Ð½Ð°Ñ‚ Ð¾Ð´ Ð±ÐµÐ·Ð±ÐµÐ´Ð½Ð¾ÑÐ½Ð¸ Ð¿Ñ€Ð¸Ñ‡Ð¸Ð½Ð¸ Ð¸ ÑƒÑˆÑ‚Ðµ ÐºÐ¾Ð¸ Ðµ Ð¼Ð¾Ð¶Ð½Ð¾ Ð´Ð° ÑÐµ Ð¾Ð´Ð»Ð¾Ð¶Ð°Ñ‚.\r\n\r\nÐ—Ð°ÐµÐ´Ð½Ð¾ Ð³Ð¾ Ð´Ð¾Ð³Ð¾Ð²Ð°Ñ€Ð°Ð¼Ðµ ÐºÐ¾Ð½ÐºÑ€ÐµÑ‚Ð½Ð¸Ð¾Ñ‚ Ð¾Ð±ÐµÐ¼ Ð½Ð° Ð¿Ð¾Ð¿Ñ€Ð°Ð²ÐºÐ°.\r\n\r\nÐžÐ´ Ð½Ð°Ñ ÑœÐµ Ð´Ð¾Ð±Ð¸ÐµÑ‚Ðµ Ð¿Ñ€ÐµÐ¿Ð¾Ñ€Ð°ÐºÐ° Ð·Ð° Ð¾Ð±ÐµÐ¼Ð¾Ñ‚ Ð½Ð° Ð¿Ð¾Ð¿Ñ€Ð°Ð²ÐºÐ°Ñ‚Ð° - Ð’Ð¸Ðµ Ð¾Ð´Ð»ÑƒÑ‡ÑƒÐ²Ð°Ñ‚Ðµ ÑˆÑ‚Ð¾ ÑœÐµ ÑÐµ Ð¸Ð·Ð²Ñ€ÑˆÐ¸. ÐÐ°ÑˆÐ¸Ð¾Ñ‚ ÑÐ¾Ð²ÐµÑ‚Ð½Ð¸Ðº Ð·Ð° ÑÐµÑ€Ð²Ð¸Ñ Ð¼Ð¾Ð¶Ðµ Ð´Ð° Ð’Ð¸ Ð³Ð¸ ÐºÐ°Ð¶Ðµ Ñ‚Ñ€Ð¾ÑˆÐ¾Ñ†Ð¸Ñ‚Ðµ Ð²Ð¾ Ð²Ñ€ÑÐºÐ° ÑÐ¾ Ð²Ð¸Ð´Ð»Ð¸Ð²Ð¸Ñ‚Ðµ Ð¾ÑˆÑ‚ÐµÑ‚ÑƒÐ²Ð°ÑšÐ°. ÐÐ° Ñ‚Ð¾Ñ˜ Ð½Ð°Ñ‡Ð¸Ð½ Ð¼Ð¾Ð¶ÐµÑ‚Ðµ Ð¿Ð¾Ð´Ð¾Ð±Ñ€Ð¾ Ð´Ð° Ð¿Ð»Ð°Ð½Ð¸Ñ€Ð°Ñ‚Ðµ Ð¸ Ð´Ð° ÑÐµ Ð¾Ð´Ð»ÑƒÑ‡Ð¸Ñ‚Ðµ.');

-- --------------------------------------------------------

--
-- Table structure for table `Contact`
--

CREATE TABLE `Contact` (
  `id` int(11) NOT NULL,
  `about_id` int(11) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `linkedin` varchar(255) DEFAULT NULL,
  `google` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Contact`
--

INSERT INTO `Contact` (`id`, `about_id`, `mobile`, `location`, `facebook`, `twitter`, `linkedin`, `google`) VALUES
(66, 70, '+389 70 *** ***', '#', 'https://www.facebook.com/', 'https://www.twitter.com/', 'https://www.linkedin.com/', 'https://plus.google.com/'),
(67, 71, '', '', '', '', '', ''),
(68, 72, '', '', '', '', '', ''),
(69, 73, '02/2626-266', 'Ð±ÑƒÐ». Ð‘Ð¾ÑÐ½Ð° Ð¸ Ð¥ÐµÑ€Ñ†ÐµÐ³Ð¾Ð²Ð¸Ð½Ð° Ð±Ñ€. 4, 1000 Ð¡ÐºÐ¾Ð¿Ñ˜Ðµ', 'https://www.facebook.com/volkswagenmakedonija/', 'https://twitter.com/', 'https://www.linkedin.com/', 'https://plus.google.com/');

-- --------------------------------------------------------

--
-- Table structure for table `Products`
--

CREATE TABLE `Products` (
  `id` int(11) NOT NULL,
  `about_id` int(11) DEFAULT NULL,
  `picture1` varchar(255) DEFAULT NULL,
  `description1` text,
  `picture2` varchar(255) DEFAULT NULL,
  `description2` text,
  `picture3` varchar(255) DEFAULT NULL,
  `description3` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Products`
--

INSERT INTO `Products` (`id`, `about_id`, `picture1`, `description1`, `picture2`, `description2`, `picture3`, `description3`) VALUES
(42, 70, 'https://uploads.dailyvanity.sg/wp-content/uploads/2016/11/earth-life-organics-face-wash.jpg', 'In three variants: Aspara Glades (aloe vera and apple scented), Eveâ€™s Garden (mangosteen scented), and Sun Daze (patchouli scented), this gentle cleanser contains vitamin E, which helps hydrate skin while offering antioxidant, anti-ageing, and antiseptic benefits. Using pure and mild ingredients, it is gentle even on delicate skin. This retails at SGD14 with the ongoing promotion. ', 'https://uploads.dailyvanity.sg/wp-content/uploads/2016/11/earth-life-organics-shower-gel-green-fields.jpg', 'Like all products by Earth Life Organics, its shower gel is formulated with a coconut oil base to offer natural nourishment. Also enriched with antioxidant-rich vitamin E and a blend of essential oils, this improves skin texture by exfoliating dead skin cells, and hydrates and nourishes skin while eliminating unpleasant odour. It retails at SGD12.90 after the discount.\r\n\r\nBesides Green Fields (green tea), this shower gel also comes in these variants: Aspara Glades (apple and aloe vera), Eveâ€™s Garden (fruity), Milky Way (rose and vanilla), Morning Mocha (coffee and chocolate), and Sun Daze (patchouli).', 'https://uploads.dailyvanity.sg/wp-content/uploads/2016/11/earth-life-organics-shampoo.jpg', 'This shampoo doesnâ€™t just strengthen weak and damaged hair, it also nourishes and regenerates scalp and hair follicles to deliver better scalp health. Infusing essential oils that have a calm effect, it is a treat for those who lead hectic lifestyles to soothe your body and mind after a tiring day. It retails at SGD15 with the ongoing promotion.\r\n\r\nFind your favourite amongst Aspara Glades (apple and aloe vera), Eveâ€™s Garden (fruity), and Purple Haze (lavender)');

-- --------------------------------------------------------

--
-- Table structure for table `Services`
--

CREATE TABLE `Services` (
  `id` int(11) NOT NULL,
  `about_id` int(11) DEFAULT NULL,
  `picture1` varchar(255) DEFAULT NULL,
  `description1` text,
  `picture2` varchar(255) DEFAULT NULL,
  `description2` text,
  `picture3` varchar(255) DEFAULT NULL,
  `description3` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Services`
--

INSERT INTO `Services` (`id`, `about_id`, `picture1`, `description1`, `picture2`, `description2`, `picture3`, `description3`) VALUES
(1, 71, '', '', '', '', '', ''),
(2, 72, '', '', '', '', '', ''),
(3, 73, 'https://www.volkswagen.com.mk/media/Theme_Teaser_Abstract_Image_Component/15246-45340-left-image/dh-363-5a4acc/c909b3ab/1477516307/service-consultant-stamping-officejpg.jpg', 'ÐŸÐ ÐžÐ’Ð•Ð ÐšÐ\r\nVolkswagen Ðµ Ð²Ð¸ÑÑ‚Ð¸Ð½ÑÐºÐ¾ Ñ‡ÑƒÐ´Ð¾ Ð½Ð° Ñ‚ÐµÑ…Ð½Ð¸ÐºÐ°Ñ‚Ð°. Ð—Ð° Ð´Ð¾Ð»Ð³Ð¾ Ð´Ð° ÑÐµ Ñ€Ð°Ð´ÑƒÐ²Ð°Ñ‚Ðµ Ð¸ Ð±ÐµÐ·Ð±ÐµÐ´Ð½Ð¾ Ð´Ð° Ð¿Ð°Ñ‚ÑƒÐ²Ð°Ñ‚Ðµ, Ð·Ð° ÑÐµÑ€Ð²Ð¸ÑÐ¸Ñ€Ð°ÑšÐµÑ‚Ð¾ Ð¿Ð¾Ñ‚Ð¿Ñ€ÐµÑ‚Ðµ ÑÐµ Ð½Ð° Ð½Ð°ÑˆÐ¸Ñ‚Ðµ ÑÐ¿ÐµÑ†Ð¸Ñ˜Ð°Ð»Ð¸ÑÑ‚Ð¸', 'https://www.volkswagen.com.mk/media/Theme_Teaser_Abstract_Image_Component/15246-45340-center-image/dh-363-5a4acc/b8e68748/1477516307/vw-pickerl-begutachtung.jpg', 'ÐŸÐ ÐžÐ’Ð•Ð ÐšÐ ÐÐ ÐÐÐ›Ð•ÐŸÐÐ˜Ð¦Ð\r\nÐŸÐµÑ€Ñ„ÐµÐºÑ‚Ð½Ð¾ Ð¾Ð±ÑƒÑ‡ÐµÐ½Ð¸Ñ‚Ðµ ÑÐ¾Ñ€Ð°Ð±Ð¾Ñ‚Ð½Ð¸Ñ†Ð¸ Ð³Ð¾ Ð°Ð½Ð°Ð»Ð¸Ð·Ð¸Ñ€Ð°Ð°Ñ‚ Ð’Ð°ÑˆÐµÑ‚Ð¾ Ð²Ð¾Ð·Ð¸Ð»Ð¾ Ð¾Ð´ Ð³ÑƒÐ¼Ð¸Ñ‚Ðµ, Ð¿Ñ€ÐµÐºÑƒ Ð³ÑƒÑ€Ñ‚Ð½Ð¸Ñ‚Ðµ Ð·Ð° ÑÐ¾Ð¿Ð¸Ñ€Ð°Ñ‡ÐºÐ¸ Ð´Ð¾ Ð°ÐºÑ‚ÑƒÐµÐ»Ð½Ð¸Ñ‚Ðµ ÐµÐ»ÐµÐºÑ‚Ñ€Ð¾Ð½ÑÐºÐ¸ ÑÐ¸ÑÑ‚ÐµÐ¼Ð¸. ÐÐ° Ñ‚Ð¾Ñ˜ Ð½Ð°Ñ‡Ð¸Ð½ Â§57a ÑÑ‚Ñ€ÑƒÑ‡Ð½Ð¾Ñ‚Ð¾ Ð¼Ð¸ÑÐ»ÐµÑšÐµ Ð’Ð¸ Ð³Ð°Ñ€Ð°Ð½Ñ‚Ð¸Ñ€Ð° Ð±ÐµÐ·Ð±ÐµÐ´Ð½Ð¾ÑÑ‚ Ð²Ð¾ ÑÐ¾Ð¾Ð±Ñ€Ð°ÑœÐ°Ñ˜Ð¾Ñ‚ Ð¸ Ð¾Ð¿ÐµÑ€Ð°Ñ‚Ð¸Ð²Ð½Ð° Ð±ÐµÐ·Ð±ÐµÐ´Ð½Ð¾ÑÑ‚ Ð½Ð° Ð’Ð°ÑˆÐµÑ‚Ð¾ Ð²Ð¾Ð·Ð¸Ð»Ð¾', 'https://www.volkswagen.com.mk/media/Theme_Teaser_Abstract_Image_Component/15246-45341-center-image/dh-425-5a4acc/db441c8f/1477516307/vw-direktannahme.jpg', 'Ð”Ð˜Ð Ð•ÐšÐ¢ÐÐž ÐŸÐ Ð˜ÐœÐÐŠÐ•\r\nÐÐ°ÑˆÐ¸Ð¾Ñ‚ ÑÐ¾Ð²ÐµÑ‚Ð½Ð¸Ðº Ð·Ð° ÑÐµÑ€Ð²Ð¸Ñ ÑœÐµ Ð¾Ð´Ð³Ð¾Ð²Ð¾Ñ€Ð¸ Ð½Ð° Ð’Ð°ÑˆÐ¸Ñ‚Ðµ Ð¿Ñ€Ð°ÑˆÐ°ÑšÐ° Ð¸ ÑÐ¾ Ð’Ð°Ñ ÑœÐµ Ñ€Ð°Ð·Ð³Ð¾Ð²Ð°Ñ€Ð° Ð·Ð° Ð´ÐµÑ‚Ð°Ð»Ð¸Ñ‚Ðµ ÐºÐ¾Ð¸ÑˆÑ‚Ð¾ ÑÐµ Ð´Ð¸Ñ€ÐµÐºÑ‚Ð½Ð¾ Ð²Ð¾ Ð²Ñ€ÑÐºÐ° ÑÐ¾ Ð²Ð¾Ð·Ð¸Ð»Ð¾Ñ‚Ð¾ Ð¸ ÑœÐµ Ð²Ð¸ Ð´Ð°Ð´Ðµ Ð¿Ñ€ÐµÐ´Ð»Ð¾Ð³ Ð·Ð° Ñ‚Ñ€Ð¾ÑˆÐ¾Ñ†Ð¸Ñ‚Ðµ. ÐÐ° Ñ‚Ð¾Ñ˜ Ð½Ð°Ñ‡Ð¸Ð½ Ð¼Ð¾Ð¶ÐµÑ‚Ðµ Ð´Ð° ÑÐµ Ð¾Ð´Ð»ÑƒÑ‡Ð¸Ñ‚Ðµ, ÑˆÑ‚Ð¾ ÑÐ°ÐºÐ°Ñ‚Ðµ Ð´Ð° ÑÐµ Ð½Ð°Ð¿Ñ€Ð°Ð²Ð¸.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `About`
--
ALTER TABLE `About`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Contact`
--
ALTER TABLE `Contact`
  ADD PRIMARY KEY (`id`),
  ADD KEY `about_id` (`about_id`);

--
-- Indexes for table `Products`
--
ALTER TABLE `Products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `about_id` (`about_id`);

--
-- Indexes for table `Services`
--
ALTER TABLE `Services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `about_id` (`about_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `About`
--
ALTER TABLE `About`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `Contact`
--
ALTER TABLE `Contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT for table `Products`
--
ALTER TABLE `Products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `Services`
--
ALTER TABLE `Services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Contact`
--
ALTER TABLE `Contact`
  ADD CONSTRAINT `Contact_ibfk_1` FOREIGN KEY (`about_id`) REFERENCES `About` (`id`);

--
-- Constraints for table `Products`
--
ALTER TABLE `Products`
  ADD CONSTRAINT `Products_ibfk_1` FOREIGN KEY (`about_id`) REFERENCES `About` (`id`);

--
-- Constraints for table `Services`
--
ALTER TABLE `Services`
  ADD CONSTRAINT `Services_ibfk_1` FOREIGN KEY (`about_id`) REFERENCES `About` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
