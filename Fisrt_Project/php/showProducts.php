<?php

class ShowProducts 
{
	private $connection;

	public function __construct()
	{
	 	$username = 'root';
		$password = 'jokiniki1234%';
	    $database_type = 'mysql';
		$database_host = 'localhost';
		$database_name = 'Website';
		
		$this->connection = new PDO("$database_type:host=$database_host;dbname=$database_name",
		$username, $password);
	}

	public function showPicture1($id)
	{
		$statement = $this->connection->prepare('SELECT Products.picture1 FROM Products JOIN About On Products.about_id = About.id WHERE About.id = :id');
		$statement->bindParam(':id', $id);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result['picture1'];
	}

	public function showDescription1($id)
	{
		$statement = $this->connection->prepare('SELECT Products.description1 FROM Products JOIN About On Products.about_id = About.id WHERE About.id = :id');
		$statement->bindParam(':id', $id);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result['description1'];
	}

	public function showPicture2($id)
	{
		$statement = $this->connection->prepare('SELECT Products.picture2 FROM Products JOIN About On Products.about_id = About.id WHERE About.id = :id');
		$statement->bindParam(':id', $id);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result['picture2'];
	}

	public function showDescription2($id)
	{
		$statement = $this->connection->prepare('SELECT Products.description2 FROM Products JOIN About On Products.about_id = About.id WHERE About.id = :id');
		$statement->bindParam(':id', $id);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result['description2'];
	}

	public function showPicture3($id)
	{
		$statement = $this->connection->prepare('SELECT Products.picture3 FROM Products JOIN About On Products.about_id = About.id WHERE About.id = :id');
		$statement->bindParam(':id', $id);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result['picture3'];
	}

	public function showDescription3($id)
	{
		$statement = $this->connection->prepare('SELECT Products.description3 FROM Products JOIN About On Products.about_id = About.id WHERE About.id = :id');
		$statement->bindParam(':id', $id);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result['description3'];
	}
}
?>