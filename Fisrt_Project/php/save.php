<?php

include 'datawriter.php';
$cover = $_POST['cover'];
$title = $_POST['title'];
$subtitle = $_POST['subtitle'];
$description = $_POST['description'];
$moreInfo = $_POST['moreInfo'];

$Website = new DataWriter();

$about_id = $Website->saveAbout($cover, $title, $subtitle, $description,$moreInfo);

$mobile = $_POST['mobile'];
$location = $_POST['location'];
$facebook = $_POST['facebook'];
$twitter = $_POST['twitter'];
$linkedin = $_POST['linkedin'];
$google = $_POST['google'];

$Website->saveContact($about_id,$mobile,$location,$facebook,$twitter,$linkedin,$google);
 
$offer = $_POST['offers']; 
$picture1 = $_POST['picture1'];
$description1 = $_POST['description1'];
$picture2 = $_POST['picture2'];
$description2 = $_POST['description2'];
$picture3 = $_POST['picture3'];
$description3 = $_POST['description3'];


if($offer == "services")
{
	$Website->saveServices($about_id, $picture1, $description1, $picture2, $description2, $picture3, $description3);
	header('Location: ../html/template.php?id='.$about_id.'&offer='.$offer);
}else
{
	$Website->saveProducts($about_id, $picture1, $description1, $picture2, $description2, $picture3, $description3);
	header('Location: ../html/template.php?id='.$about_id.'&offer='.$offer);
}

?>