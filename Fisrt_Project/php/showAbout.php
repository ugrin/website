<?php

class ShowAbout 
{
	private $connection;

	public function __construct()
	{
	 	$username = 'root';
		$password = 'jokiniki1234%';
	    $database_type = 'mysql';
		$database_host = 'localhost';
		$database_name = 'Website';
		
		$this->connection = new PDO("$database_type:host=$database_host;dbname=$database_name",
		$username, $password);
	}

	public function showCover($id)	
	{
		$statement = $this->connection->prepare('SELECT link_cover FROM About WHERE id = :id');
		$statement->bindParam(':id', $id);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result['link_cover'];
	}

	public function showTitle($id)	
	{
		$statement = $this->connection->prepare('SELECT title FROM About WHERE id = :id');
		$statement->bindParam(':id', $id);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result['title'];
	}

	public function showSubtitle($id)	
	{
		$statement = $this->connection->prepare('SELECT subtitle FROM About WHERE id = :id');
		$statement->bindParam(':id', $id);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result['subtitle'];
	}

	public function showDescription($id)	
	{
		$statement = $this->connection->prepare('SELECT description FROM About WHERE id = :id');
		$statement->bindParam(':id', $id);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result['description'];
	}

	public function showMoreInfo($id)	
	{
		$statement = $this->connection->prepare('SELECT moreInfo FROM About WHERE id = :id');
		$statement->bindParam(':id', $id);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result['moreInfo'];
	}
}
?>