<?php

class ShowServices
{
	private $connection;

	public function __construct()
	{
	 	$username = 'root';
		$password = 'jokiniki1234%';
	    $database_type = 'mysql';
		$database_host = 'localhost';
		$database_name = 'Website';
		
		$this->connection = new PDO("$database_type:host=$database_host;dbname=$database_name",
		$username, $password);
	}

	public function showPicture1($id)
	{
		$statement = $this->connection->prepare('SELECT Services.picture1 FROM Services JOIN About On Services.about_id = About.id WHERE About.id = :id');
		$statement->bindParam(':id', $id);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result['picture1'];
	}

	public function showDescription1($id)
	{
		$statement = $this->connection->prepare('SELECT Services.description1 FROM Services JOIN About On Services.about_id = About.id WHERE About.id = :id');
		$statement->bindParam(':id', $id);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result['description1'];
	}

	public function showPicture2($id)
	{
		$statement = $this->connection->prepare('SELECT Services.picture2 FROM Services JOIN About On Services.about_id = About.id WHERE About.id = :id');
		$statement->bindParam(':id', $id);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result['picture2'];
	}

	public function showDescription2($id)
	{
		$statement = $this->connection->prepare('SELECT Services.description2 FROM Services JOIN About On Services.about_id = About.id WHERE About.id = :id');
		$statement->bindParam(':id', $id);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result['description2'];
	}

	public function showPicture3($id)
	{
		$statement = $this->connection->prepare('SELECT Services.picture3 FROM Services JOIN About On Services.about_id = About.id WHERE About.id = :id');
		$statement->bindParam(':id', $id);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result['picture3'];
	}

	public function showDescription3($id)
	{
		$statement = $this->connection->prepare('SELECT Services.description3 FROM Services JOIN About On Services.about_id = About.id WHERE About.id = :id');
		$statement->bindParam(':id', $id);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result['description3'];
	}
}
?>