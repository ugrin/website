<?php 

class DataWriter
{
	private $connection;

	public function __construct()
	{
	 	$username = 'root';
		$password = 'jokiniki1234%';
	    $database_type = 'mysql';
		$database_host = 'localhost';
		$database_name = 'Website';
		
		$this->connection = new PDO("$database_type:host=$database_host;dbname=$database_name",
		$username, $password);
	}

	public function saveAbout($linkCover, $title, $subtitle, $description, $moreInfo)
	{
		$statement = $this->connection->prepare('INSERT INTO About(link_cover,title,subtitle,description,moreInfo)
	 	VALUES (:link_cover,:title,:subtitle,:description,:moreInfo)');

		$statement->bindParam(':link_cover', $linkCover);
		$statement->bindParam(':title', $title);
		$statement->bindParam(':subtitle', $subtitle);
		$statement->bindParam(':subtitle', $subtitle);
		$statement->bindParam(':description', $description);
		$statement->bindParam(':moreInfo', $moreInfo);
		$statement->execute();

		return $id = $this->connection->lastInsertId();
	}

	public function saveContact($about_id, $mobile, $location, $facebook, $twitter, $linkedin, $google)
	{
		$statement = $this->connection->prepare('INSERT INTO Contact(about_id,mobile,location,facebook,twitter,linkedin,google)
		VALUES (:about_id, :mobile, :location, :facebook, :twitter, :linkedin, :google)');

		$statement->bindParam(':about_id', $about_id);
		$statement->bindParam(':mobile', $mobile);
		$statement->bindParam(':location', $location);
		$statement->bindParam(':facebook', $facebook);
		$statement->bindParam(':twitter', $twitter);
		$statement->bindParam(':linkedin', $linkedin);
		$statement->bindParam(':google', $google);
		$statement->execute();
	}

	public function saveProducts($about_id, $picture1, $description1, $picture2, $description2, $picture3, $description3)
	{
		$statement = $this->connection->prepare('INSERT INTO Products(about_id,picture1,description1,picture2,description2,picture3,description3)
		VALUES (:about_id, :picture1, :description1, :picture2, :description2, :picture3, :description3)');

		$statement->bindParam(':about_id', $about_id);
		$statement->bindParam(':picture1', $picture1);
		$statement->bindParam(':description1', $description1);
		$statement->bindParam(':picture2', $picture2);
		$statement->bindParam(':description2', $description2);
		$statement->bindParam(':picture3', $picture3);
		$statement->bindParam(':description3', $description3);
		$statement->execute();
	}

	public function saveServices($about_id, $picture1, $description1, $picture2, $description2, $picture3, $description3)
	{
		$statement = $this->connection->prepare('INSERT INTO Services(about_id,picture1,description1,picture2,description2,picture3,description3)
		VALUES (:about_id, :picture1, :description1, :picture2, :description2, :picture3, :description3)');

		$statement->bindParam(':about_id', $about_id);
		$statement->bindParam(':picture1', $picture1);
		$statement->bindParam(':description1', $description1);
		$statement->bindParam(':picture2', $picture2);
		$statement->bindParam(':description2', $description2);
		$statement->bindParam(':picture3', $picture3);
		$statement->bindParam(':description3', $description3);
		$statement->execute();
	}
}

