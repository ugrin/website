<?php

class ShowContact 
{
	private $connection;

	public function __construct()
	{
	 	$username = 'root';
		$password = 'jokiniki1234%';
	    $database_type = 'mysql';
		$database_host = 'localhost';
		$database_name = 'Website';
		
		$this->connection = new PDO("$database_type:host=$database_host;dbname=$database_name",
		$username, $password);
	}

	public function showMobile($id)	
	{
		$statement = $this->connection->prepare('SELECT Contact.mobile FROM Contact JOIN About On Contact.about_id = About.id WHERE About.id = :id');
		$statement->bindParam(':id', $id);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result['mobile'];
	}

	public function showLocation($id)	
	{
		$statement = $this->connection->prepare('SELECT Contact.location FROM Contact JOIN About On Contact.about_id = About.id WHERE About.id = :id');
		$statement->bindParam(':id', $id);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result['location'];
	}

	public function showFacebook($id)	
	{
		$statement = $this->connection->prepare('SELECT Contact.facebook FROM Contact JOIN About On Contact.about_id = About.id WHERE About.id = :id');
		$statement->bindParam(':id', $id);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result['facebook'];
	}

	public function showTwitter($id)	
	{
		$statement = $this->connection->prepare('SELECT Contact.twitter FROM Contact JOIN About On Contact.about_id = About.id WHERE About.id = :id');
		$statement->bindParam(':id', $id);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result['twitter'];
	}

	public function showLinkedin($id)	
	{
		$statement = $this->connection->prepare('SELECT Contact.linkedin FROM Contact JOIN About On Contact.about_id = About.id WHERE About.id = :id');
		$statement->bindParam(':id', $id);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result['linkedin'];
	}

	public function showGoogle($id)	
	{
		$statement = $this->connection->prepare('SELECT Contact.google FROM Contact JOIN About On Contact.about_id = About.id WHERE About.id = :id');
		$statement->bindParam(':id', $id);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result['google'];
	}
}
?>