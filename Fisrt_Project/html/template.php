<?php
	include '../php/showAbout.php';
	include '../php/showContact.php';
	
	$id = $_GET['id'];
	$offer = $_GET['offer'];

	$a = new ShowAbout();	
	$c = new ShowContact();

	if($offer == "services")
	{
		include '../php/showServices.php';
		$o = new ShowServices();
	}
	else
	{
		include '../php/showProducts.php';
		$o = new ShowProducts();
	}

	function offer($offer)
	{
		 if($offer == "services") 
		 { 
		 	echo "services";
		 }else{ 
		 	echo "products";
		 }
	}
?>

<html>
<head>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	
	<meta name="viewport" content="width=device-width, initial-scale=1">

	
	<link href="https://fonts.googleapis.com/css?family=Bree+Serif|Istok+Web|Signika" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<title><?php echo $a->showTitle($id);?></title>

	<style type="text/css">
		.first {
			background: black;
    		overflow: hidden;
    		position: fixed;
  			top: 0;
  			width: 100%;
  			z-index: 10000;		
		}
		.first ul {
			margin-top: 10px;
		}
		.first li a{
			padding-right: 10px;
			text-decoration: none;
			color: white;
		}
		.first li a:hover {
			background-color: white;
			color: black;
			padding: 10px 5px;	
		}
		/*********************header************************/
		.fa{
 		 	padding: 5px;
    		font-size: 10px;
   			width: 25px;
    		text-align: center;
    		text-decoration: none;
    		border-radius: 10%;
		}
		.fa:hover {
    		opacity: 0.7;
		}
		.fa-facebook {
 			background: #3B5998;
  			color: white;
		}
		.fa-twitter {
 			background: #55ACEE;
  			color: white;
		}
		.fa-google {
 			background: #dd4b39;
  			color: white;
		}
		.fa-linkedin {
  			background: #007bb5;
  			color: white;
		}
		/************************linkovi***********************/
		.cover h1 {
			padding-top: 180px;
			font-weight: bold;
			color: white;
			font-family: 'Bree Serif', serif;
			font-size: 48px;
			letter-spacing: 3px;
		}
		.cover h2 {
			font-weight:bold; color:white; font-family: 'Bree Serif', serif;"
		}
		.cover{
			padding-left: 0px;
			padding-right: 0px;
		}
		/************************cover*************************/
		.fourth div {
			padding-top: 200px;
    		padding-bottom: 200px;
    		border: 10px solid white;
    		border-radius: 5px;
		}
		.box{
			background-position: center;
			background-size: cover;
		}
		/***********************fourth row************************/
		.footer {
			background: black;
			color:white;
			margin-top: 5px;
		}
		.footer ul {
			margin-top: 10px;
		}
		.seventh {
			margin-right:5px; 
		}
		.seventh .form{
			border: 2px solid black;
			padding: 10px;
			background: blue;
			color: black;
		}
		/**********************footer*****************************/
	</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row first">
			<div class="col-md-12">
				<ul class="list-unstyled list-inline">
					<li><a href="#">Дома</a></li> 
					<li><a href="template.php?id=<?php echo $id;?>&offer=<?php offer($offer);?>#about">За нас</a></li> 
					<li><a href="template.php?id=<?php echo $id;?>&offer=<?php offer($offer);?>#<?php offer($offer);?>">
						<?php if($offer == "services") { echo "Сервиси";} else{ echo "Продукти";}?></a></li> 
					<li><a href="template.php?id=<?php echo $id;?>&offer=<?php offer($offer);?>#contact">Контакт</a></li> 
				</ul>
			</div>		
		</div>

		<div class="row">
			<div class="col-md-12 cover" style="background-image:url(<?php echo $a->showCover($id);?>); width: 100%;
			 height: 70%; background-position:center; background-repeat:no-repeat; background-size: cover;">
				<h1 class="text-center" ><?php echo $a->showTitle($id);?></h1>
				<h2 class="text-center" ><?php echo $a->showSubtitle($id);?></h2>
			</div>
		</div>

		<div class="row second" id="about">
			<div class="col-md-8">
				<h3 style="font-family: 'Signika', sans-serif;">За нас</h3>
				<p style="font-family: 'Istok Web', sans-serif;"><?php echo $a->showDescription($id); ?></p>
			</div>
			<div class="col-md-4 text-center">
				<h3 style="font-family: 'Signika', sans-serif;">Телефон</h3>
				<p style="font-family: 'Istok Web', sans-serif;"><?php echo $c->showMobile($id);?></p>
				<h3 style="font-family: 'Signika', sans-serif;">Локација</h3>
				<p style="font-family: 'Istok Web', sans-serif;"><?php echo $c->showLocation($id);?></p>
			</div>
		</div>

		<div class="row third">
			<div class="col-md-4 col-md-offset-4 text-center">
				<h3 id="<?php offer($offer);?>" style="font-family: 'Signika', sans-serif;"><?php if($offer == "services") { echo "Сервиси";} else{ echo "Продукти";}?></h3>
			</div>
		</div>

		<div class="row fourth">
			<div class="col-md-4 box" style="background-image:url(<?php echo $o->showPicture1($id);?>">
			</div>
			<div class="col-md-4 box" style="background-image:url(<?php echo $o->showPicture2($id);?>">
			</div>
			<div class="col-md-4 box" style="background-image:url(<?php echo $o->showPicture3($id);?>">
			</div>
		</div>

		<div class="row fifth">
			<div class="col-md-4">
				<h3 style="font-family: 'Signika', sans-serif;">Опис на првиот <?php if($offer == "services") { echo "сервис";} else{ echo "продукт";}?></h3>
				<p style="font-family: 'Istok Web', sans-serif;"><?php echo $o->showDescription1($id);?></p>
			</div>
			<div class="col-md-4">
				<h3 style="font-family: 'Signika', sans-serif;">Опис на вториот <?php if($offer == "services") { echo "сервис";} else{ echo "продукт";}?></h3>
				<p style="font-family: 'Istok Web', sans-serif;"><?php echo $o->showDescription2($id);?></p>
			</div>
			<div class="col-md-4">
				<h3 style="font-family: 'Signika', sans-serif;">Опис на третиот <?php if($offer == "services") { echo "сервис";} else{ echo "продукт";}?></h3>
				<p style="font-family: 'Istok Web', sans-serif;"><?php echo $o->showDescription3($id);?></p>
			</div>
		</div>

		<div class="row sixth" id="contact">
			<div class="col-md-4 col-md-offset-4 text-center">
				<h3 style="font-family: 'Signika', sans-serif;">Контакт</h3>
			</div>
		</div>

		<div class="row seventh">
			<div class="col-md-5">
				<h4 style="font-family: 'Signika', sans-serif;">Повеќе информации</h4>
				<p style="font-family: 'Istok Web', sans-serif;"><?php echo $a->showMoreInfo($id);?></p>
			</div>
			<div class="col-md-5 col-md-offset-2 form">
				<div class="form-group" style="font-family: 'Signika', sans-serif;">
					<label for="name">Име</label>
					<input type="text" class="form-control" id="name" placeholder="Вашето име">
			  		<label for="email">Емаил</label>
    				<input type="email" class="form-control" id="email" placeholder="Вашиот емаил">
    				<label for="message">Порака</label>
    				<textarea class="form-control" rows="5" placeholder="Вашата порака"></textarea>
  				</div>

  				<div class="col-md-6 col-md-offset-3">  					
  					<div class="form-group" style="font-family: 'Signika', sans-serif;">
    					<input type="submit" class="form-control" id="submit" value="Испрати">
    				</div>
  				</div>
  			</div>
		</div>

		<div class="row footer">
	 		<div class="col-md-8">
	 			<p>&copy Ангела Угриновска</p>
	 		</div>
	 		<div class="col-md-2 col-md-offset-2">
	 			<ul class="list-unstyled list-inline">
	 				<li><a href="<?php echo $c->showFacebook($id);?>" target="_blank" class="fa fa-facebook"></a></li>
	 				<li><a href="<?php echo $c->showTwitter($id);?>" target="_blank" class="fa fa-twitter"></a></li>
	 				<li><a href="<?php echo $c->showLinkedin($id);?>" target="_blank" class="fa fa-linkedin"></a></li>
	 				<li><a href="<?php echo $c->showGoogle($id);?>" target="_blank" class="fa fa-google"></a></li>
	 			</ul>
	 		</div>
	 	</div>	
	</div>
</body>
</html>	