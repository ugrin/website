<!DOCTYPE html>
<html>
<head>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<link href="https://fonts.googleapis.com/css?family=Bree+Serif|Signika" rel="stylesheet">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<style type="text/css">
		body {
			background-color: rgb(205, 180, 237);
		}
		.first h1 {
			font-size: 36px;
			font-family: 'Bree Serif', serif;
		}
		.second label {
			margin-top: 10px;
			letter-spacing: 1px;
		}
	</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row first">
			<div class="col-md-6 col-md-offset-3">
				<h1 class="text-center">Еден чекор ве дели од Вашата веб страна</h1>
			</div>
		</div>
	

		<div class="row second">
	 		<div class="col-md-4 col-md-offset-4">
	 			<form class="form-horizontal" role="form" method="POST" action="../php/save.php" style="font-family: 'Signika', sans-serif;
">
	  				<!-- <div class="form-group"> -->
	  					<label>Напишете го линкот до cover сликата:</label>
						<input type="text" name="cover" class="form-control"/>

						<label>Внесете го насловот:</label> 
						<input type="text" name="title" class="form-control"/>

						<label>Внесете го поднасловот:</label>
						<input type="text" name="subtitle" class="form-control"/>

						<label>Напишете нешто за вас:</label>
						<textarea class="form-control" name="description" rows="5"></textarea>

						<label>Внесете го вашиот телефон:</label>
						<input type="text" name="mobile" class="form-control"/>

						<label>Внесете ја вашата локација:</label>
						<input type="text" name="location" class="form-control"/>

						<hr>
						<label>Одберете дали нудите сервиси или продукти:</label>
						<select name="offers" class="form-control">
							<option value="services">Сервиси</option>
							<option value="products">Продукти</option>
						</select>

						<label>Внесете URL од слика и опис од вашите продукти или сервиси:</label>

						<label>URL од слика</label>
						<input type="text" name="picture1" class="form-control"/>
						<label>Опис на слика</label>
						<textarea class="form-control" name="description1" rows="5"></textarea>

						<label>URL од слика</label>
						<input type="text" name="picture2" class="form-control"/>
						<label>Опис на слика</label>
						<textarea class="form-control" name="description2" rows="5"></textarea>	

						<label>URL од слика</label>
						<input type="text" name="picture3" class="form-control"/>
						<label>Опис на слика</label>
						<textarea class="form-control" name="description3" rows="5"></textarea>
						<hr>
					
						<label>Напишете нешто за вашата фирма што луѓето треба да го знаат пред да ве запознаат:</label>
						<textarea class="form-control" name="moreInfo" rows="5"></textarea>

						<hr>
						<label>Facebook</label>
						<input type="text" name="facebook" class="form-control"/>

						<label>Twitter</label>
						<input type="text" name="twitter" class="form-control"/>

						<label>Linkedin</label>
						<input type="text" name="linkedin" class="form-control"/>

						<label>Google+</label>
						<input type="text" name="google" class="form-control"/>
						<hr>

						<button class="btn btn-default form-control" type="submit">Потврди</button>
				</form>
					<!-- </div> Kopce sto ke ne nosi na template.php?id=? "template.php?id="-->
	 		</div>
	 	</div>
	</div>
</body> 	